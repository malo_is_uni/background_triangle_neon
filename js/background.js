var canvas = document.getElementById('back');
var ctx = canvas.getContext('2d');
var d = new Date();

/************************************************************************************
*                                 cookie                                            *
************************************************************************************/ 
function getCookie(cname)
{
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');

  for(var i = 0; i <ca.length; i++)
  {
    var c = ca[i];
    while (c.charAt(0) == ' ')
    {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0)
    {
      return c.substring(name.length, c.length);
    }
  }
  return null;
}

var theme;

if ((theme = getCookie("theme")) == null)
{
  document.cookie = "theme=100; expires=" + (d.getTime() + 30 * 24 * 60 * 60 * 1000);
  theme = 100;
}
else
{
  document.cookie = "theme="+ theme + "; expires=" + (d.getTime() + 30 * 24 * 60 * 60 * 1000);
}

/************************************************************************************
*                    selection theme et changement theme                            *
************************************************************************************/
theme = parseInt(theme);
var themeswitch = document.getElementById('inputswitch');

themeswitch.checked = theme == 0;

render(theme);

// mettre a jour la taille du canva quand la page change de taille
addEventListener('resize', function()
{
  render(theme);
}, false)

// mise a jour de la couleur du theme apres lutilisation du bouton switch
themeswitch.addEventListener('click', function()
  {
    theme = (theme == 0 ? 100 : 0);
    document.cookie = "theme="+ theme + "; expires=" + (d.getTime() + 30 * 24 * 60 * 60 * 1000);
    render(theme);
  },false);





/************************************************************************************
*                        dessin canvas background                                   *
************************************************************************************/
function rnd(min, max)
{
  return Math.floor((Math.random() * (max - min + 1)) + min);
};

function render(valtheme)
{
  var canvasWidth = canvas.width = window.innerWidth;
  var canvasHeight = canvas.height = window.innerHeight;
  var tTri = 50;//taille des triangle
  var hauteurTri = 0.7; //hauteur des triangle
  var tligne = Math.floor(tTri * hauteurTri);
  var columns = Math.ceil(canvasWidth / tTri) + 1;
  var rows = Math.ceil(canvasHeight / tligne);

  var i, j;
  for (j = 0; j < rows; j++) 
  {
    for (i = 0; i < columns; i++)
    {
      var x = i * tTri;
      var y = j * tligne;

      if (y % 2 != 0)
      {
        x -= tTri / 2;
      }

      //triangle 1/2
      ctx.fillStyle = 'hsl( 0, 0%, '+ valtheme +'%)';
      ctx.strokeStyle = 'hsl(' + rnd(0, 360) + ', 50%, ' + rnd(40, 60) + '%)';
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(x + tTri / 2, y + tligne);
      ctx.lineTo(x - tTri / 2, y + tligne);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      //triangle 2/2
      ctx.fillStyle = 'hsl( 0, 0%, '+ valtheme +'%)';
      ctx.strokeStyle = 'hsl(' + rnd(0, 360) + ', 50%, ' + rnd(40, 60) + '%)';
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(x + tTri, y);
      ctx.lineTo(x + tTri / 2, y + tligne);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

    };
  };
};
